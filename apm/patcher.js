/**
 * Import this file during APM setup, using either:
 * import 'elastic-connector/apm/patcher.js'
 * or
 * require('elastic-connector/apm/patcher.js')
 *
 * It will resolve the issue of APM not reporting the Elasticsearch traffic,
 * due to different package aliases
 * (e.g. package `@elastic/elasticsearch` renamed to `elasticsearch8`)
 */
const apm = require('elastic-apm-node');

const packageNames = [
  // if you're going to add multiple versions of Elasticsearch libraries other than
  // @elastic/elasticsearch and @elastic/elasticsearch-canary
  // please add the names of the packages to this list
  // (whatever is in your package.json)
];

for (const packageName of packageNames) {
  apm.addPatch([packageName], (...args) => {
    return require(`elastic-apm-node/lib/instrumentation/modules/@elastic/elasticsearch.js`)(...args);
  });
}

