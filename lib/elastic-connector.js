const jsonpath = require('jsonpath');
const _ = require('lodash');
const elasticlib7 = require('@elastic/elasticsearch');
const elasticlib8 = require('@elastic/elasticsearch-canary');
const {errors} = require('@elastic/elasticsearch-canary');
const {InsertManyError} = require('../errors');
const ElasticScroll = require('./elastic-scroll');
const ElasticSearchDeep = require('./elastic-searchDeep');

/**
 * This class is defined as singleton
 */
class ElasticConnector {
  constructor() {
    this.elastic = null;
    this.indexConfig = null;
    this.ES_MAX_RESULTS = 2147483647; // Java's Integer.MAX_VALUE
    this.connectionParams = null;
    this._esMajorVersion = null;
  }

  get version() {
    if (!this.elastic) {
      throw new Error('`connect` method not called');
    }
    return this._esMajorVersion;
  }

  connect(connString, config, options = {}) {
    if (!connString) {
      throw new Error('Host not provided');
    }
    if (!config) {
      throw new Error('Configuration not defined');
    }

    this.indexConfig = this._setupConfig(config);
    const {nodes, auth} = this._getEsNodesList(connString);

    this.connectionParams = {
      nodes,
      auth,
      requestTimeout: options.requestTimeout || 60_000,
      sniffOnStart: typeof options.sniffOnStart === 'boolean' ? options.sniffOnStart : true,
      sniffOnConnectionFault: typeof options.sniffOnConnectionFault === 'boolean' ? options.sniffOnConnectionFault : true,
    };

    if (options.disableSecurityCheck) {
      // ES8
      this.connectionParams.tls = {checkServerIdentity: () => undefined, rejectUnauthorized: false};
      // ES7
      this.connectionParams.ssl = {checkServerIdentity: () => undefined, rejectUnauthorized: false};
    }

    return this.reconnect();
  }

  async createConnection(connString, config, options = {}) {
    const es = new ElasticConnector();
    return es.connect(connString, config, options);
  }

  async _getElasticInfo() {
    let esInfo;
    try {
      esInfo = await this.elastic.info();
    } catch (err) {
      if (err instanceof errors.ResponseError && err.statusCode === 401) {
        throw new Error('Invalid credentials');
      } else {
        throw err;
      }
    }
    return esInfo;
  }

  async reconnect() {
    let esInfo;
    try {
      this.elastic = new elasticlib8.Client(this.connectionParams);
      esInfo = await this._getElasticInfo();
    } catch (error) {
      if (error instanceof errors.ProductNotSupportedError) {
        this.elastic = new elasticlib7.Client(this.connectionParams);
        esInfo = await this._getElasticInfo();
      } else {
        throw error;
      }
    }

    esInfo = esInfo.body ? esInfo.body : esInfo;
    this._esMajorVersion = Number(esInfo.version.number.split('.')[0]);
    return this;
  }

  async health() {
    try {
      const result = await this.elastic.cluster.health({
        format: 'json',
      });

      return {
        available: true,
        status: result.body ? result.body.status : result.status,
      };
    } catch (error) {
      return {
        available: false,
        error,
      };
    }
  }

  _getEsNodesList(connString) {
    const esComponentsMatcher = /^elastics?:\/\/(?:([^:]+):([^@]+)@)?(.*)$/;
    if (!esComponentsMatcher.test(connString)) {
      throw new Error('Invalid ES connection string');
    }

    const [, username, password, nodesStr] = connString.match(esComponentsMatcher);
    const usingSsl = connString.startsWith('elastics://');

    const nodes = nodesStr.split(',').map((n) => {
      const components = n.match(/^([^:]+)(?::)?(\d+)?$/);
      if (!components) {
        throw new Error(`Invalid elasticsearch node: ${n}`);
      }
      const protocol = usingSsl ? 'https' : 'http';
      const host = components[1];
      const port = components[2] || 9200;

      return `${protocol}://${host}:${port}`;
    });

    return {
      nodes,
      auth: username && password ? {username, password}: undefined,
    };
  }

  async search(
    type,
    query,
    useMapper = true,
  ) {
    const types = [];

    if (Array.isArray(type)) {
      if (type.length === 0) {
        throw new Error('\'type\' array cannot be empty');
      }
      types.push(...type);
    } else if (typeof type === 'string') {
      types.push(type);
    } else {
      throw new Error('\'type\' can be either string or array of strings');
    }

    query = this._mapSearchRequest(types[0], query, useMapper);

    const indexes = [];
    const esTypes = [];

    types.forEach((type) => {
      const {index, type: elasticType} = this._getIndexConfig(type);

      indexes.push(index);
      esTypes.push(elasticType);
    });

    query = this._mapSearchRequest(type, query, useMapper);

    const esResponse = await this.elastic.search({
      index: indexes,
      type: this._esMajorVersion < 7 ? esTypes : undefined,
      body: query,
    });

    const resp = esResponse.body ? esResponse.body : esResponse;
    return this._mapSearchResponse(resp);
  }

  searchDeep(
    type,
    query,
    tieBreakersSortOrder = 'asc',
    useMapper = true,
    usePit = true,
    keepAlive = '10m',
  ) {
    query = this._mapSearchRequest(type, query, useMapper);

    const {index, type: elasticType, tieBreakers} = this._getIndexConfig(type);

    return new ElasticSearchDeep(
      this.elastic,
      this._esMajorVersion,
      index,
      elasticType,
      query,
      tieBreakers,
      tieBreakersSortOrder,
      usePit,
      keepAlive,
    );
  }

  async searchAfter(
    type,
    query,
    from,
    perPage,
    tieBreaker,
    sortOrder = 'desc',
  ) {
    const BULK_SIZE = 10000;
    if (from + perPage < 10000) {
      throw new Error('For searches with up to 10k results, use regular search');
    }

    let pageHits = [];

    // on the first iteration we take the last element only
    query.size = 1;
    query.from = BULK_SIZE - 1;
    query.sort ??= {};
    query.sort[tieBreaker] = sortOrder || 'desc'; // tieBreaker should have unique value
    // bulkPages refer to huge data bulks (e.g. 10_000 by 10_000)
    const bulkPages = Math.ceil((from + perPage) / BULK_SIZE);
    let itemsSearched = query.from;
    // iterating over bulk pages
    // after each iteration the sort property of last element is used for the `search_after` operation
    // the `_source` is not fetched until the last page since it is not necessary (this reduces response time significantly)
    for (let index = 0; index < bulkPages; index++) {
      const isLastPage = index === bulkPages - 1;
      // we need `_source` only for the last page
      query._source = isLastPage;

      if (isLastPage) {
        // smaller optimization - lowering query size if last page
        query.size = from + perPage - itemsSearched;
      }
      const esResponse = await this.search(type, query);
      const currentItemsCount = esResponse.hits.hits.length;
      itemsSearched += currentItemsCount;

      // the requested page exceeds the number of pages in the result
      if (currentItemsCount === 0) {
        pageHits = [];
        break;
      }

      // on the next iteration, search after the last element from previous iteration
      query['search_after'] = esResponse.hits.hits[esResponse.hits.hits.length - 1].sort;
      query.size = BULK_SIZE;

      // from parameter is not allowed to be used with `search_after`
      delete query.from;

      if (isLastPage) {
        pageHits = esResponse.hits.hits;
        const sliceFrom = from % BULK_SIZE;
        if (sliceFrom === 0 && esResponse.hits.hits.length === perPage) {
          // no need to slice anything
          pageHits = esResponse.hits.hits;
        } else {
          // slicing array to fit into required boundaries
          pageHits = esResponse.hits.hits.slice(sliceFrom, sliceFrom + perPage);
        }
      }
    }
    return {pageHits, itemsSearched};
  }

  async insert(type, document, id = null) {
    const {index, type: elasticType} = this._getIndexConfig(type);
    const insertObject = {
      index,
      type: this._esMajorVersion < 7 ? elasticType : undefined,
      body: document,
    };
    if (id) {
      insertObject.id = id;
    }
    const result = await this.elastic.index(insertObject);
    return result.body ? result.body : result;
  }

  async remove(type, id) {
    if (!id) {
      throw new Error('Please specify ID when removing a document');
    }
    const {index, type: elasticType} = this._getIndexConfig(type);
    const deleteObject = {
      index,
      type: this._esMajorVersion < 7 ? elasticType : undefined,
      id,
    };
    const result = await this.elastic.delete(deleteObject);
    return result.body ? result.body : result;
  }

  async updatePartially(type, document, id) {
    const {index, type: elasticType} = this._getIndexConfig(type);
    const updateObject = {
      index,
      type: this._esMajorVersion < 7 ? elasticType : undefined,
      id,
      body: {doc: document},
    };
    const result = await this.elastic.update(updateObject);
    return result.body ? result.body : result;
  }

  async insertMany(type, docs, idGetter = null, maxRetries = 10) {
    let currentRetry = 0;
    if (!Array.isArray(docs)) {
      throw new Error('elastic.insertMany: docs is not an Array');
    }
    if (docs.length === 0) {
      return;
    }
    const {index, type: elasticType} = this._getIndexConfig(type);
    const bulkBody = [];
    const action = {
      index: {
        _index: index,
        _type: this._esMajorVersion < 7 ? elasticType : undefined,
      },
    };
    for (const doc of docs) {
      let id = null;
      if (typeof idGetter === 'string') {
        id = _.get(doc, idGetter);
      } else if (typeof idGetter === 'function') {
        id = idGetter(doc);
      }

      if (id) {
        // adding ID to the document
        bulkBody.push({index: Object.assign({_id: id}, action.index)});
      } else {
        bulkBody.push(action);
      }
      bulkBody.push(doc);
    }

    let bulkResult;
    do {
      currentRetry++;
      bulkResult = await this.elastic.bulk({body: bulkBody});
      bulkResult = bulkResult.body ? bulkResult.body : bulkResult;

      if (!bulkResult.errors) {
        return; // no errors, which means everything succeeded
      }
      const failedIds = [];
      for (const [id, result] of bulkResult.items.entries()) {
        if (result.index.error) {
          failedIds.push(id);
        }
      }

      // now, map failed id's to objects that we tried to insert but they failed
      docs = failedIds.map((id) => docs[id]);
    } while (currentRetry < maxRetries);

    if (bulkResult.errors) {
      const bulkErrors = bulkResult.items.flatMap((e) => {
        return e.index.error ? e.index.error : [];
      });
      throw new InsertManyError(docs, bulkErrors);
    }
  }

  async multiGet(type, ids, onlySource = true) {
    if (ids.length === 0) {
      return Promise.resolve([]);
    }
    const {index, type: elasticType} = this._getIndexConfig(type);

    const results = await this.elastic.mget({
      index,
      type: this._esMajorVersion < 7 ? elasticType : undefined,
      body: {
        ids,
      },
    });
    const resultBody = results.body ? results.body : results;
    return onlySource ? resultBody.docs.map((r) => r._source) : resultBody;
  }

  async get(type, id, onlySource = true) {
    const {index, type: elasticType} = this._getIndexConfig(type);
    try {
      let result = await this.elastic.get({
        index,
        type: this._esMajorVersion < 7 ? elasticType : undefined,
        id,
      });
      result = result.body ? result.body : result;
      return onlySource ? result._source : result;
    } catch (err) {
      if (err?.meta?.statusCode === 404) {
        return null; // document not found
      } else {
        throw err;
      }
    }
  }

  async count(type, query = null, useMapper = true) {
    const {index} = this._getIndexConfig(type);
    const o = {
      index,
    };
    if (query) {
      query = this._mapSearchRequest(type, query, useMapper);
      o.body = query;
    }
    const results = await this.elastic.count(o);
    return results.body ? results.body.count : results.count;
  }

  scroll(type, query, duration = '5m', useMapper = true) {
    query = this._mapSearchRequest(type, query, useMapper);

    const {index, type: elasticType} = this._getIndexConfig(type);
    return new ElasticScroll(
      this.elastic,
      this._esMajorVersion,
      index,
      elasticType,
      query,
      duration,
    );
  }

  _getIndexConfig(type) {
    const indexConfig = this.indexConfig[type];
    if (!indexConfig?.index) {
      throw new Error(
        `No defined index name for specified type ${type}`,
      );
    }
    return {
      ...indexConfig,
      type: indexConfig.type ?? type,
    };
  }

  _mapSearchRequest(type, query, useMapper) {
    if (this._esMajorVersion < 7 && (query.aggs || query.aggregations)) {
      // make query compatible
      const aggs = query.aggs ?? query.aggregations;
      // Elasticsearch 6: replace date_histogram's `calendar_interval` and `fixed_interval` with `interval`
      for (const dh of jsonpath.query(aggs, '$..date_histogram')) {
        if (dh.fixed_interval) {
          dh.interval = dh.fixed_interval;
          dh.fixed_interval = undefined;
        } else if (dh.calendar_interval) {
          dh.interval = dh.calendar_interval;
          dh.calendar_interval = undefined;
        }
      }
    }

    if (useMapper) {
      const indexData = this.indexConfig[type];
      if (typeof indexData?.queryMapper === 'function') {
        query = indexData.queryMapper(query);
      }
    }

    return query;
  }

  _mapSearchResponse(resp) {
    if (this._esMajorVersion >= 7) {
      return resp;
    }
    const totalHits = resp.hits.total;
    resp.hits.total = {
      value: totalHits,
      relation: 'eq',
    };
    return resp;
  }

  _setupConfig(config) {
    config = _.cloneDeep(config);
    _.forEach(config, (c) => {
      if (c.tieBreakers && typeof c.tieBreakers === 'string') {
        c.tieBreakers = [c.tieBreakers];
      }
    });
    return config;
  }
}

module.exports = ElasticConnector;
