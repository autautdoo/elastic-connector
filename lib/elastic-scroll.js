class ElasticScroll {
  constructor(elastic, esMajorVersion, index, type, query, scrollDuration) {
    this._elastic = elastic;
    this._esMajorVersion = esMajorVersion;
    this._index = index;
    this._type = type;
    this._query = query;
    this._limit = Infinity;
    this._duration = scrollDuration;
    this._scrollId = null;
    this._isDone = false;
    this._nitems = 0;

    if (!this._query.size) {
      this._query.size = 10000;
    }
  }

  [Symbol.asyncIterator]() {
    return this.iterator(true);
  }

  async* iterator(onlySource = true) {
    while (!this.isDone) {
      const docs = await this.getItems();
      for (const doc of docs.hits.hits) {
        yield onlySource ? doc._source : doc;
      }
    }
  }

  async getItems() {
    if (this._isDone) {
      throw new Error(
        'ElasticScroll: scroll exhausted. Please use `isDone` getter before using `getItems`',
      );
    }
    let results;
    if (this._scrollId) {
      results = await this._elastic.scroll({
        body: {
          scroll: this._duration,
          scroll_id: this._scrollId,
        },
      });
    } else {
      results = await this._elastic.search({
        index: this._index,
        type: this._esMajorVersion < 7 ? this._type : undefined,
        scroll: this._duration,
        body: this._query,
      });
    }

    results = results.body ? results.body : results;
    this._scrollId = results._scroll_id;
    const resultsHits = results.hits ? results.hits : results;
    this._nitems += resultsHits.hits.length;
    if (this._nitems >= this._limit || resultsHits.hits.length === 0) {
      this._isDone = true;
      this.dispose();
      if (this._nitems > this._limit) {
        resultsHits.hits.splice(this._limit % resultsHits.hits.length);
      }
    }
    return results;
  }

  get isDone() {
    return this._isDone;
  }

  limit(n) {
    this._limit = n;
    if (this._query.size >= n) {
      this._query = Object.assign({}, this._query, {size: n});
    }
    return this;
  }

  get itemsScrolled() {
    return this._nitems;
  }

  /**
   * Called automatically if limit is set and reached
   */
  dispose() {
    return this._elastic.clearScroll({
      body: {
        scroll_id: this._scrollId,
      },
    });
  }
}

module.exports = ElasticScroll;
