class ElasticSearchDeep {
  constructor(elastic, esVersion, index, type, query, tieBreakers, tieBreakersSortOrder, usePit = true, keepAlive= '10m') {
    this._elastic = elastic;
    this._tieBreakers = tieBreakers;
    this._tieBreakersSortOrder = tieBreakersSortOrder;
    this._type = type;
    this._esVersion = esVersion;
    this._query = {...query};
    this._index = index;
    this._keepAlive = keepAlive;
    this._usePit = usePit;
    this._pit = null;
    this._isDone=false;
    this._nitems = 0;
    this.prepareQuery();
  }

  [Symbol.asyncIterator]() {
    return this.iterator(true);
  }

  prepareQuery() {
    if (!this._tieBreakers) {
      throw new Error('tieBreakers param is required');
    }
    if (!this._query.size) {
      this._query.size = 10000;
    }
    let sortArray = [];
    // sort is not an array, let's make it one
    if (!Array.isArray(this._query.sort)) {
      for (const [property, order] of Object.entries(this._query.sort ?? {})) {
        sortArray.push({[property]: order});
      }
    } else {
      // copy array items, so we don't modify original query
      sortArray = [...this._query.sort];
    }

    // add tie-breakers
    for (const key of this._tieBreakers) {
      sortArray.push({[key]: this._tieBreakersSortOrder});
    }
    this._query.sort = sortArray;
  }

  async* iterator(onlySource = true) {
    while (!this.isDone) {
      const docs = await this.getItems();
      for (const doc of docs.hits.hits) {
        yield onlySource ? doc._source : doc;
      }
    }
  }

  async searchWithPit() {
    if (!this._pit) {
      // create new PIT
      const pitData = await this._elastic.openPointInTime({index: this._index, keep_alive: this._keepAlive});
      this._pit = pitData.body ? pitData.body.id : pitData.id;
    }
    this._query.pit = {
      'id': this._pit,
      'keep_alive': this._keepAlive,
    };
    const esResponse = await this._elastic.search({
      type: this._esVersion < 7 ? this._type : undefined,
      body: this._query,
    });
    const body = esResponse.body ? esResponse.body : esResponse;
    // update pit from the most recent search page
    this._pit = body.pit_id;
    return body;
  }

  async searchWithoutPit() {
    const esResponse = await this._elastic.search({
      index: this._index,
      type: this._esVersion < 7 ? this._type : undefined,
      body: this._query,
    });
    return esResponse.body ? esResponse.body : esResponse;
  }

  async getItems() {
    if (this._isDone) {
      throw new Error('SearchDeep is done. Please use isDone method before calling getItems');
    }
    let results;
    if (this._esVersion >= 7 && this._usePit) {
      results = await this.searchWithPit();
    } else {
      results = await this.searchWithoutPit();
    }
    const hitsCount = results.hits.hits.length;
    this._nitems += hitsCount;
    // if we don't get any hits or we get less hits than the query size, we can be sure that there is not more data
    if (hitsCount === 0 || hitsCount < this._query.size) {
      this._isDone = true;
      this.dispose();
      return results ?? [];
    }
    this._query.search_after = results.hits.hits[results.hits.hits.length - 1].sort;

    return results;
  }

  get isDone() {
    return this._isDone;
  }

  get query() {
    return JSON.stringify(this._query);
  }

  dispose() {
    if (this._usePit && this._pit) {
      return this._elastic.closePointInTime({body: {id: this._pit}});
    }
  }

  get itemsIterated() {
    return this._nitems;
  }
}

module.exports = ElasticSearchDeep;
