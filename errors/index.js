class BaseError extends Error {
  constructor(message) {
    super(`[elastic-connector] ${message}`);
  }
}

class InsertManyError extends BaseError {
  constructor(failedDocs, errors) {
    super(`insertMany error: ${failedDocs.length} docs failed to insert`);
    this.failedDocs = failedDocs;
    this.errors = errors;
  }
}

module.exports = {
  InsertManyError,
};
