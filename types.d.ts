import {ApiResponse, Client} from "@elastic/elasticsearch";
import {TransportRequestPromise} from "@elastic/elasticsearch/lib/Transport";

declare namespace ElasticConnectorLib {
  type ElasticQuery = object
  type SortOrder = 'asc' | 'desc'

  interface IndexConfig {
    index: string
    type?: string
    rollupIndex?: string
    queryMapper?: (ElasticQuery) => ElasticQuery
  }

  interface ClientOptions {
    requestTimeout?: number
    sniffOnConnectionFault?: boolean
  }

  interface HealthStatus {
    /** If connection is established */
    available: boolean
    /** The elastic cluster health status */
    status?: 'green' | 'yellow' | 'red'
    /** Error while getting cluster health */
    error?: Error
  }

  type ValuesOf<T extends any[]> = T[number];

  type IndexConfigs = {
    [key: string]: IndexConfig;
  }

  type IdGetter = string | ((object: object) => number | string)

  class ElasticScroll {
    constructor(
      elastic: Client,
      esMajorVersion: number,
      index: string,
      type: string,
      query: ElasticQuery,
      scrollDuration: string | undefined,
    );

    [Symbol.asyncIterator](): AsyncIterator

    iterator(onlySource: boolean = true): AsyncIterator

    getItems(): Promise<any>

    get isDone(): boolean

    limit(n: number): this

    get itemsScrolled(): number

    /**
     * Called automatically if limit is set and reached
     */
    dispose(): TransportRequestPromise<ApiResponse<T.ClearScrollResponse, unknown>>
  }

  class ElasticSearchDeep {
    constructor(
      elastic: Client,
      esMajorVersion: number,
      index: string,
      type: string,
      query: ElasticQuery,
      tieBreakers: string | string[],
      tieBreakersSortOrder: SortOrder = 'asc',
      usePit: boolean = true,
      keepAlive: string = "10m"
    );

    [Symbol.asyncIterator](): AsyncIterator

    iterator(onlySource: boolean = true): AsyncIterator

    getItems(): Promise<any>

    query(): string

    get isDone(): boolean

    /**
     * Called automatically if limit is set and reached
     */
    dispose(): TransportRequestPromise<ApiResponse<T.TransportRequestCallback, unknown>>

    get itemsIterated(): number
  }

  interface SearchAfterResponse {
    /** Documents for requested page */
    pageHits: Array<ApiResponse['body']>
    /** Number of searched records */
    itemsSearched: number
  }

  class ElasticConnector {
    constructor()

    elastic: Client

    ES_MAX_RESULTS: 2147483647

    /**
     * Connects current ElasticConnector instance to the ES cluster
     *
     * @param connString Comma separated list of host[:port]
     * @param config     Indices configuration
     * @param options    Object which contains various setup options.
     */
    connect(connString: string, config: IndexConfigs, options: ClientOptions = {}): Promise<this>

    /**
     * Creates a new ElasticConnector instance and connects it to the specified ES cluster.
     * This might be useful if you need to use more than one ES cluster.
     * If you're using this method (instead of `connect`),
     * it is required to keep a reference to the returned value.
     *
     * @param connString Comma separated list of host[:port]
     * @param config Indices configuration
     * @param options Object which contains various setup options.
     *        These include:
     *          * requestTimeout (default 60000)
     *          * sniffOnConnectionFault (default true)
     * @returns {Promise<ElasticConnector>}
     */
    createConnection(connString: string, config: IndexConfigs, options: ClientOptions = {}): Promise<this>

    reconnect(): Promise<this>

    /**
     * Returns the health status of a cluster
     *
     * @return {HealthStatus}
     */
    health(): Promise<HealthStatus>

    /**
     * This method searches index of given entity type
     *
     * @param type Type(s) of data you want to search, e.g. 'bets', 'agents', etc.
     * @param query Query object
     * @param useMapper If set to false, query will not go through mapper
     */
    search(type: string | string[], query: ElasticQuery, useMapper: boolean = true): Promise<ApiResponse['body']>

    /**
     * This method is used to `scroll` through elastic but without using elastic search scroll, rather searchAfter functionality.
     *
     * @param types      Type of data you want to search through, e.g. 'bets', 'agents', ['bets', 'sportbets']
     * @param query      Query object
     * @param tieBreakersSortOrder How the tiebreaker is going to sort results. Either ascending or descending
     * @param useMapper  Use document mapper, used by default
     * @param usePit     Determines if we use the PIT (Point in time) elastic feature (PIT available only on es >= 7)
     * @param keepAlive  Determines how long should the PIT be alive on es
     */
    searchDeep(types: string, query: ElasticQuery, tieBreakersSortOrder: SortOrder = 'asc', useMapper: boolean = true, usePit: boolean = true, keepAlive: string = '10m'): ElasticSearchDeep

    /**
     * This method searches through huge amount of data using bulk pages.
     * After each iteration through pages, the `sort` parameter is taken from the last element.
     * This parameter is then used with the ES `search_after` option to go to the next bulk page.
     * The `_source` parameter is set to false until the last page since the data from former pages is not needed.
     * This method returns the number of iterated records and hits for the requested page.
     *
     * @param type        Type(s) of data you want to search, e.g. 'bets', 'agents'
     * @param query       Query object
     * @param from        Number of documents to skip. Used to determine from which record to search
     * @param perPage     Used to determine how many records to return
     * @param tieBreaker  Tiebreaker is unique value that is used for performing `search_after` operation
     * @param sortOrder   Sort order, ascending or descending
     *
     * @return Resolved promise is an object which contains number of searched records
     *         and the hits for requested page
     */
    searchAfter(type: string | string[], query: ElasticQuery, from: number, perPage: number, tieBreaker: string, sortOrder: SortOrder = 'desc'): Promise<SearchAfterResponse>

    /**
     * Insert or replace (if id is given) a document of given entity type
     *
     * @param type      Type of data you want to insert, e.g. 'bets', 'agents'
     * @param document  Document data to insert
     * @param id        Optional document id. If not given, it will be autogenerated
     */
    insert(type: string, document: object, id: string = null): Promise<ApiResponse['body']>

    /**
     * Delete a document of given entity type by id
     *
     * @param type  Type of data you want to insert, e.g. 'bets', 'agents'
     * @param id    Document id, required
     */
    remove(type: string, id: string): Promise<ApiResponse['body']>

    /**
     * Partially update a document of given entity type by id
     *
     * @param type      Type of data you want to insert, e.g. 'bets', 'agents'
     * @param document  Partial data to update document with
     * @param id        Document id, required
     */
    updatePartially(type: string, document: object, id: string): Promise<ApiResponse['body']>

    /**
     * Insert multiple documents of given entity type
     *
     * @param type        Type of data you want to insert, e.g. 'bets', 'agents'
     * @param docs        Array of document data to insert
     * @param idGetter    Document id, or a (synchronous) function which returns document id
     * @param maxRetries  How many retries to perform for failed inserts. 10 by default.
     */
    insertMany(type: string, docs: object[], idGetter: IdGetter = null, maxRetries: number = 10): Promise<ApiResponse['body']>

    /**
     * Fetches multiple documents by their IDs
     *
     * @param type Type of data you want to insert, e.g. 'bets', 'agents'
     * @param ids Array of document IDs
     * @param onlySource Return only _source data from search
     */
    multiGet(type: string, ids: string[] | number[], onlySource: boolean = true): Promise<ApiResponse['body']>

    /**
     * Get document by id for given entity type. Equivalent to Elasticsearch's `GET <index>/_doc/<_id>` API.
     *
     * @param type        Type of data you want to get, e.g. 'bets', 'agents'
     * @param id          Document id, required
     * @param onlySource  Return only _source data from search
     */
    get(type: string, id: string | number, onlySource: boolean = true): Promise<ApiResponse['body']>

    /**
     * Count number of documents of given entity type, optionally with a provided query.
     *
     * @param type       Type of data you want to get, e.g. 'bets', 'agents'
     * @param query      Query object
     * @param useMapper  Use document mapper, used by default.
     */
    count(type: string, query: ElasticQuery = null, useMapper: boolean = true): Promise<number>

    /**
     * Create an elasticsearch scroll. It is disposed automatically.
     *
     * @param type      Type of data you want to scroll through, e.g. 'bets', 'agents'
     * @param query     Query object
     * @param duration  Period to keep the scroll alive, 5 minutes by default
     * @param useMapper Use document mapper, used by default
     */
    scroll(type: string, query: ElasticQuery, duration: string = '5m', useMapper: boolean = true): ElasticScroll
  }
}

export = new ElasticConnectorLib.ElasticConnector
