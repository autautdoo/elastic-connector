# Changelog
All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).
[comment]: <> (Types of changes: Added, Changed, Deprecated, Removed, Fixed, Optimized, Security)

## Unreleased

## 6.1.1
### Added
- IGPIMP-3450 - Add support for multiple indexes in `search` function

## 6.1.0
### Added
 - IGPIMP-3377 - Node discovery sniffOnStart config option
### Changed
 - Connection Authentication trough `auth` property, instead of connection string 

## 6.0.1
### Fixed
 - IGPIMP-3345 - Fix sorting in ElasticSearchDeep.prepareQuery function

## 6.0.0
### Added
- IGPIMP-2949 - Rollup search functionality (`searchRollup` method)
### Changed
- Signature of following methods (mostly because of removed date arguments):
  - `search`
  - `searchDeep`
  - `searchAfter`
  - `scroll`
  - `insert`
  - `insertMany`
  - `remove`
  - `updatePartially`
### Removed
- method `getIndexName`
- arguments `fromDate` & `toDate` from search, insert, update and delete document methods (affected methods are listed above)
- Index date spanning
- option to search multiple types in one query
- index read & write policies and aliasing
- files in `test/` directory

## 5.0.2
### Fixed
- Fix APM instrumentation for elasticsearch (no need to import apm patcher)

## 5.0.1
### Fixed
- IGPSUP-3727 - Fix APM instrumentation for Elasticsearch

## 5.0.0
### Added
- IGPIMP-2897 - `elastic-connector` is now compatible with `elasticsearch` 8
- IGPIMP-2899 - Queries are now mapped from ES 7/8 format to ES 6 format if older version of ES is used

## 4.5.1
### Fixed
- IGPIMP-2886 - Fixed a bug with document type in `searchDeep`

## 4.5.0
### Added
- IGPIMP-2886 - `itemsIterated` counter for searchDeep function

## 4.4.0
### Added
- IGPIMP-2881 - New `createConnection` method that allows user to create connections to multiple Elasticsearch clusters

## 4.3.0
### Added
- IGPIMP-2765 - Implemented `searchDeep` method that mimics scroll using searchAfter functionality.

## 4.2.4
### Fixed
- Possible TypeError in "document not found" error handler.
  Property `meta` is not always available on the error, e.g. when we get no response from elastic,
  this will throw the error further up in such cases - instead of causing a TypeError

## 4.2.3
### Fixed
- IGPSUP-3075 - 
  Raising exception if trying to use `searchAfter` with `from` & `size` that can be used in regular search.
  Using search after with those parameters caused a bug anyway.
  Also, Relying on total hits (which is only >10000 if `track_total_hits` is provided). 
  Applied some minor optimizations 

## 4.2.2
### Fixed
- IGPIMP-2081 - Verifying whether the requested page exceeds the number of available pages in `searchAfter` method

## 4.2.1
### Fixed
- IGPSUP-2987 - Elasticsearch scroll id is too big to be sent as a part of URI. Sending it in body instead

## 4.2.0
### Added
- IGPIMP-2054
  - Added method descriptions to type definitions
  - Improved/fixed type definitions
  - Updated dependencies
  - Removed NoLivingConnectionsError handler

## 4.1.0
### Added
- IGPIMP-2025 - Added `searchAfter` method for deep pagination

## 4.0.0
### Added
- Library throws an error on Elasticsearch `insertMany` fails
- Some of the more specific errors that library throws can be found in `errors` directory.
  You can have them in your app by importing (requiring) `elastic-connector/errors`
### Changed
- Brought back compatibility with Node.js 12
- InsertMany now throws an `InsertManyError`
### Removed
- Library logging & log levels
- Following methods: `searchIndex`, `searchMany`
- `usePokerBets` argument when querying elasticsearch
### Fixed
- A bug with retries inside `insertMany`
- `insertMany` will no longer fail if provided with empty documents array

## 3.0.1
### Changed
- Updated config parameter type

## 3.0.0
### Changed
- IGPIMP-1863:
  - Enabled sniffing on connection fault by default
  - Lowered timeout to 60 seconds
  - Added types (index names are not inferred correctly though)
  - Changed config structure
  - Library now requires nodejs version >=14

## 2.0.2
### Added
- Cluster `health` method

## 2.0.1
### Added
- Yet more simpler way of iterating through objects, which utilizes `Symbol.asyncIterator`.
  Example usage:
  ```javascript
  for await (const doc of elastic.scroll(entity, query).limit(50000)) {
    // doc is returned as a _source
  }
  ```
  You can still use `Scroll.iterator()` method, if you for example need to access document metadata.

## 2.0.0
### Added
- Added new method `iterator` on elastic scroll which returns iterator
### Changed
- Library is able to communicate with both ES6 & ES7. ES version is detected automatically
- `connect` & `reconnect` methods now returns a promise
- New ES connection format, in form of:
  `elastic[s]://[username:password@]ip1[:port1][,ip2[:port2][,...]]`
  Examples:
  `elastic://192.168.3.10,192.168.3.11:9900`
  `elastic://uname:pwd@192.168.3.12,192.168.3.13,192.168.3.14`
  `elastics://magicaluser:magicalpwd@search-magicalspin.eu-west-1.es.amazonaws.com`
- Scroll `getItems` funciton now returns exact amount of items if `limit` method was used
### Removed
- The following methods:
    * `setTemplate`
    * `dropTemplate`
    * `indexExists`
    * `aliasExists`
    * `drop`
    * `setAlias`
### Optimized
- Potentially improving scroll performance by defining a default size of 10000 items, 
  unless you explicitly change it by calling `limit` method  

## 1.2.2
### Fixed
- `insertMany` bugfix - `idGetter` argument was always falling back to null

## 1.2.1
### Fixed
- `insertMany` bugfixes - Retry now works as intended. Elastic type no longer causes issues

## 1.2.0
### Changed
- IGPSUP-1135 - Reconnecting if no living connection error is thrown while performing some Elasticsearch operation

## 1.1.0
### Changed
- Library is now able to connect to multiple comma-separated hosts
- Renamed package name back to `elastic-connector`
- Upgraded to new Elasticsearch library
- Converted promises to async-await
- Updated dependencies to latest versions

## 1.0.5
### Fixed
- fixed linter errors
